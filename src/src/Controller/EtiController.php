<?php

namespace App\Controller;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class EtiController
 */
class EtiController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function blogHomepage()
    {
        return $this->render('eti/blog/homepage.html.twig');
    }

    /**
     * @Route("/first/page", name="first_page")
     * @param TranslatorInterface $translator
     * @return Response
     * @throws Exception
     */
    public function randomNumber(TranslatorInterface $translator)
    {
        $number = random_int(0, 100);

        return $this->render('eti/blog/first_page.html.twig', [
            'number' => $number,
            'translated_php' => $translator->trans('Translated string'),
            'translated_php_pl' => $translator->trans('Translated string',[], 'messages', 'pl_PL')
        ]);
    }
}